<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .map{
                position: fixed;
                top: 0;
                bottom:0;
                left: 0;
                right: 0;
                background-color: #000;
                height: 100%;

            }
        </style>
    </head>
    <body>

         <div class="map" id="map">
            
        </div>
        
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtG1XzPW2anPAVF12tLOpMP-NwQxXT7Es&callback=initMap"
    async defer></script>

    <script>
        var myLatLng = {lat :16.3849919,lng:44.2296681};
        var map;
        var markers = [];
        var infos = [];
        var bounds;
        var initMap = function(){
            map = new google.maps.Map(document.getElementById('map'),{
                center: myLatLng,
                zoom: 8
            });
        }

        fetch('http://localhost:8000/api/drivers').then(function(drivers){
            return drivers.json()
        }).then(function(drivers){
            console.log(drivers )
            goToCreateMap(drivers);
            
        });


        var goToCreateMap = function(drivers){
            bounds = new google.maps.LatLngBounds();
            drivers.forEach(function(driver,index){
                bounds.extend({lat : parseFloat(driver.lat) , lng : parseFloat(driver.lng)});
                markers.push({
                    marker : new google.maps.Marker({
                        position : {lat : parseFloat(driver.lat) , lng : parseFloat(driver.lng)},
                        map : map,
                        title : driver.name
                    }),
                    driver : driver.str_id

                })  
                infos.push({
                    info : new google.maps.InfoWindow({
                        content : driver.name,
                    }),
                });
                infos[index].info.open(map , markers[index].marker);
            });

            map.fitBounds(bounds);


        }
    </script>

        <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
      <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('48c6e43bf18802c1b9f6', {
          cluster: 'us2',
          forceTLS: true
        });

        var channel = pusher.subscribe('driver-server-move');
        channel.bind('driver-server-move-event', function(data) {
                var driver = data.driver.driver;
                bounds = new google.maps.LatLngBounds();

                // markers.forEach(function(m,i){
                //     if(m.marker.str_id === dr.str_id){
                //         markers[i].marker.setPostion({lat : parseFloat(dr.lat) , lng : parseFloat(dr.lng)})
                //         bounds.extend({lat : parseFloat(dr.lat) , lng : parseFloat(dr.lng)});
                //     } else {
                //         bounds.extend(m.marker.getPosition());
                //     }

                // });

                markers = markers.map(function(marker){
                if(marker.driver === driver.str_id){
                    marker.marker.setPosition({lat : parseFloat(driver.lat),lng : parseFloat(driver.lng)})  
                         map.panTo({lat : parseFloat(driver.lat),lng : parseFloat(driver.lng)});
                            // bounds.extend(m.marker.getPosition());

                } 
                    bounds.extend(marker.marker.getPosition());

                return marker;
                })
                map.fitBounds(bounds);


        });
    </script>
    </body>
</html>
