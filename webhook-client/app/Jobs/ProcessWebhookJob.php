<?php
namespace App\Jobs;

use App\Events\DriverMove;
use \Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;

class ProcessWebhookJob extends SpatieProcessWebhookJob
{
    public function handle()
    {
        // $this->webhookCall // contains an instance of `WebhookCall`
    
        // perform the work here

        event(new DriverMove($this->webhookCall->payload));
    }
}