<?php

namespace App\Listeners;

use App\Events\DriverMove;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DriverMoveListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverMove  $event
     * @return void
     */
    public function handle(DriverMove $event)
    {
        //
    }
}
