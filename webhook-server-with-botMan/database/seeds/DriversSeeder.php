<?php

use Illuminate\Database\Seeder;

class DriversSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    	$drivers = [
    		[
    			'name'=>'Hisham Ahmed',
    			'lat' => '15.3849919',
    			'lng' => '44.2296681',
    			'str_id'=>'hisham'
    		],[
    			'name'=>'Ahmed Wesam',
    			'lat' => '15.3849919',
    			'lng' => '44.2296681',
    			'str_id'=>'ahmed'
    		],[
    			'name'=>'Rhman Doctor',
    			'lat' => '15.3849919',
    			'lng' => '44.2296681',
    			'str_id'=>'rhman'
    		],[
    			'name'=>'Rosa Bosa',
    			'lat' => '15.3849919',
    			'lng' => '44.2296681',
    			'str_id'=>'rosa'
    		],
    	];
        \App\Driver::create($drivers);
    }
}
