<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>How to install Botman Chatbot in Laravel 5? - ItSolutionStuff.com</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/assets/css/chat.min.css">
 
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            body{
                background-image:none !important;
            }
            .map{
                position: fixed;
                top: 0;
                bottom:0;
                left: 0;
                right: 0;
                background-color: #000;
                height: 100%;

            }
        </style>
    </head>
    <body>
        <div class="map" id="map">
            
        </div>
    </body>
  
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/assets/css/chat.min.css">
    <script>
        var botmanWidget = {
            aboutText: 'ssdsd',
            introMessage: "✋ Hi! I'm form ItSolutionStuff.com"
        };
    </script>
  
    <script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>


      <script src="https://maps.googleapis.com/maps/api/js?key={{ config("api.GOOGLE_MAP_API") }}&callback=initMap"
    async defer></script>
    <script>
        var myLatLng = {lat :16.3849919,lng:44.2296681};
        var map;
        var markers = [];
        var infos = [];
        var initMap = function(){
            map = new google.maps.Map(document.getElementById('map'), {
              center: myLatLng,
              zoom: 8
            });
        

        fetch('./api/drivers').then(function(drivers){
            return drivers.json()
        }).then(function(drivers){
            console.log(drivers )
            goToCreateMap(drivers);
            
        });

        


        

        
        





        }

        var goToCreateMap = function(drivers){
            drivers.forEach(function(driver,index){
                markers.push({
                    marker : new google.maps.Marker({
                        position : {lat : parseFloat(driver.lat) , lng : parseFloat(driver.lng)},
                        map : map,
                        title : driver.name
                    }),
                    driver : driver.str_id

                })  
                infos.push({
                    info : new google.maps.InfoWindow({
                        content : driver.name,
                    }),
                });
                infos[index].info.open(map , markers[index].marker);
            });
        }   



    </script>

    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
      <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
          cluster: 'us2',
          forceTLS: true
        });

        var channel = pusher.subscribe('driver-showen');
        channel.bind('driver-showen-event', function(data) {
          // alert(JSON.stringify(driver));
          driver = data.driver;
          console.log(data);
          // map.panTo()
          console.log((driver.lat));
          map.panTo({lat : parseFloat(driver.lat),lng : parseFloat(driver.lng)});
          

          
        });
        var channel2 = pusher.subscribe('driver-change-place');
          channel2.bind('driver-change-place-evnet' , function(data){
            driver = data.driver;
            console.log(driver);
            markers = markers.map(function(marker){
                if(marker.driver === driver.str_id){
                    marker.marker.setPosition({lat : parseFloat(driver.lat),lng : parseFloat(driver.lng)})  
                map.panTo({lat : parseFloat(driver.lat),lng : parseFloat(driver.lng)});

                }
                return marker;
            })
          })
      </script>
      
</html>