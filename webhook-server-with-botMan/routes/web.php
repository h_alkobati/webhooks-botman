<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Google\Cloud\Dialogflow\V2\SessionsClient;
// use Google\Cloud\Dialogflow\V2\TextInput;
// use Google\Cloud\Dialogflow\V2\QueryInput;

use Google\Cloud\Dialogflow\V2\IntentsClient;
use Google\Cloud\Dialogflow\V2\Intent;
use Google\Cloud\Dialogflow\V2\Intent\TrainingPhrase\Part;
use Google\Cloud\Dialogflow\V2\Intent\TrainingPhrase;
use Google\Cloud\Dialogflow\V2\Intent\Message;
use Google\Cloud\Dialogflow\V2\Intent\Message\Text;

Route::get('/', function () {
	// detect_intent_texts('newagent-iehwqn','how are you?','123456');
// 	$intentsClient = new IntentsClient();
// 	$parent = $intentsClient->projectAgentName("newagent-iehwqn");
// 	$intents = $intentsClient->listIntents($parent,array("intentView"=>1));
// 	$allIntents = array();
// 	$iterator = $intents->getPage()->getResponseObject()->getIntents()->getIterator();
// 	while($iterator->valid()){
// 	$intent = $iterator->current();
// 	$allIntents[] = array("id"=>$intent->getName(),"name"=>$intent->getDisplayName());
// 	$iterator->next();
// }
// return Response::json(array('success'=>true,'allIntents'=>$allIntents));
	return view('welcome');


    	
});


Route::match(['get', 'post'], '/botman', 'BotManController@handle');



function detect_intent_texts($projectId, $text, $sessionId, $languageCode = 'en-US')
{
    // new session
    $test = array('credentials' => '../j.json');
    $sessionsClient = new SessionsClient($test);
    $session = $sessionsClient->sessionName($projectId, $sessionId ?: uniqid());
    printf('Session path: %s' . PHP_EOL, $session);

    // create text input
    $textInput = new TextInput();
    $textInput->setText($text);
    $textInput->setLanguageCode($languageCode);

    // create query input
    $queryInput = new QueryInput();
    $queryInput->setText($textInput);

    // get response and relevant info
    $response = $sessionsClient->detectIntent($session, $queryInput);
    $queryResult = $response->getQueryResult();
    $queryText = $queryResult->getQueryText();
    $intent = $queryResult->getIntent();
    $displayName = $intent->getDisplayName();
    $confidence = $queryResult->getIntentDetectionConfidence();
    $fulfilmentText = $queryResult->getFulfillmentText();

    // output relevant info
    print(str_repeat("=", 20) . PHP_EOL);
    printf('Query text: %s' . PHP_EOL, $queryText);
    printf('Detected intent: %s (confidence: %f)' . PHP_EOL, $displayName,
        $confidence);
    print(PHP_EOL);
    printf('Fulfilment text: %s' . PHP_EOL, $fulfilmentText);

    $sessionsClient->close();
}