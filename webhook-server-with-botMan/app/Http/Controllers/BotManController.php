<?php

namespace App\Http\Controllers;

use App\Traits\Geocode;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Middleware\ApiAi;
use Illuminate\Http\Request;


class BotManController extends Controller
{
    use Geocode ;
    public function handle()
    {
        $botman = app('botman');
  		$dialogflow = ApiAi::create(config('api.API_AI'));
        $botman->middleware->received($dialogflow);

		
		$botman->hears('hello there', function (BotMan $bot) {
		
		    $this->processBot($bot);
		    // $bot->reply($apiReply);
		})->middleware($dialogflow);
		$botman->listen();
    }


    private function processBot($bot){
    		$extras = $bot->getMessage()->getExtras();
		    
		    $apiReply = $extras['apiReply'];
		    $apiAction = $extras['apiAction'];
		    $apiIntent = $extras['apiIntent'];

	    	$str_id = (strtolower(@$extras['apiParameters']['Drivers']));
		    if($str_id)
		    $driver  = \App\Driver::where('str_id',$str_id)->first();
	    	
		    

		    if(@$extras['apiIntent'] === 'Driver Place'){
		    	
		    	$this->whereIsTheDriver($driver);


		    	$bot->reply($apiReply.":".$this->geolocationaddress($driver->lat,$driver->lng));
		    }else if(@$extras['apiIntent'] === 'Move Driver'){
		    	

		    	$this->moveDriver($extras,$str_id,$driver);
		    	
		    	$bot->reply($apiReply);

		    }
		    else{
		    	$bot->reply($apiReply);
		    }
    }

    private function whereIsTheDriver($driver){
		event(new \App\Events\ShowMapEvent($driver));
    }


    private function moveDriver($extras ,$str_id,$driver){
    			$move_to = (strtolower($extras['apiParameters']['any']));
		    	$new_address = $this->geolocationaddress(null,null,$move_to);
		    	
		    	$driver_update  = \App\Driver::where('str_id',$str_id)->update(
		    		$new_address
		    	);

		    	
		    	event(new \App\Events\DriverPlaceChangeEvent($driver));
    }
  
 
}
