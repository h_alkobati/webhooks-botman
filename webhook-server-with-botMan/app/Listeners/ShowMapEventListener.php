<?php

namespace App\Listeners;

use App\Events\ShowMapEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShowMapEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShowMapEvent  $event
     * @return void
     */
    public function handle(ShowMapEvent $event)
    {
        //
    }
}
