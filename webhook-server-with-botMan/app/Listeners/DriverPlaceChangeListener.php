<?php

namespace App\Listeners;

use App\Events\DriverPlaceChangeEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Spatie\WebhookServer\WebhookCall;

class DriverPlaceChangeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverPlaceChangeEvent  $event
     * @return void
     */
    public function handle(DriverPlaceChangeEvent $event)
    {
        
        
        
        WebhookCall::create()
           ->url('http://localhost:8001/driver-place-change')
           ->payload(['driver' => $event->driver])
           ->useSecret('hishamsecrit')
           ->dispatch();
    }
}
